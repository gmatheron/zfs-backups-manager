cd /root/zbm

python3 zbm/zbm.py prune \
  --local-zfs-root main/bak-zbm \
  --prefix bak-zbm- \
  --keep-last 5 \
  --keep-hourly 24 \
  --keep-daily 30 \
  --keep-monthly 12 \
  -y

python3 zbm/zbm/zbm.py verify \
  --local-zfs-root main/bak-zbm \
  --prefix bak-zbm- \
  --max-age-days 1 \
  --min-snapshots 20
