# ZFS Backups Manager

## Description

ZFS Backups Manager (ZBM) is a python script that automates creating and managing backups of ZFS volumes using snapshots.
It takes advantage of snapshots and `zfs send`, which is more efficient than the built-in mechanism of PVE that is storage-agnostic but relies on frequent full-volume scans for deduplication.

It was intended to be used with [Proxmox Virtual Environment](https://www.proxmox.com/en/proxmox-ve) VMs, but can manage any ZFS volume.

The reference architecture for this project is:
1- a set of PVE cluster nodes (source) that host VMs. A cron job calls ZBM, which sends the current state of each vm to a backup server running an independent ZFS pool outside of the cluster (dest).
2- optionally, the backup server can then replicate these backups using ZBM to any number of offsite servers (replica).
3- all backups servers can use ZBM to prune and verify the backups


In step 1, a couple of snapshots may be kept on the source nodes to allow incremental replication of future snapshots, making this system fully incremental and very disk and storage-efficient.


## Example

On source servers running PVE:

```
# Backup VMs to zbm-dest
python3 zbm.py backup-pve \
  --remote zbm-dest \
  --prefix bak-zbm- \
  --local-zfs-root rpool/data \
  --remote-zfs-root main/bak-zbm \
  --remote-metadata-dir /root/backups_metadata \
  -y
```

On destination server `zbm-dest` running a bare Debian with an OpenZFS pool:

```
# Replicate backups to zbm-replica
python3 zbm.py replicate \
  --remote zbm-replica \
  --remote-zfs-root main/bak-zbm \
  --local-zfs-root main/bak-zbm

# Prune local backups, ensuring we keep non-replicated backups,
# and a snapshot in common with the replica
python3 zbm.py prune \
  --local-zfs-root main/bak-zbm \
  --prefix bak-zbm- \
  --keep-last 2 \
  --keep-hourly 2 \
  --remote zbm-replica \
  --remote-zfs-root main/bak-zbm \
  -y

python3 zbm.py verify \
  --local-zfs-root main/bak-alphabet \
  --prefix bak-alphabet- \
  --max-age-days 1 \
  --min-snapshots 2
```

On replica server `zbm-replica`:

```
python3 zbm.py prune \
  --local-zfs-root main/bak-zbm \
  --prefix bak-zbm- \
  --keep-last 5 \
  --keep-hourly 24 \
  --keep-daily 30 \
  --keep-monthly 12 \
  -y

python3 zbm.py verify \
  --local-zfs-root main/bak-zbm \
  --prefix bak-zbm- \
  --max-age-days 1 \
  --min-snapshots 20
```


## Installation

```
git clone [THIS REPOSITORY] zbm
python3 -m venv venv
source venv/bin/activate
pip install poetry
cd zbm
poetry install
python zbm.py --help
```


## Usage

```
usage: PROG [-h] [--verbose] {backup-pve,prune,replicate,verify} ...

positional arguments:
  {backup-pve,prune,replicate,verify}
                        sub-command help
    backup-pve          Backup PVE VMs based on ZFS volumes from the local system to a backup server through a SSH tunnel
    prune               Prune backup snapshots according to a schedule. Optionally preserve snapshots that have not yet been synced to an offsite storage server.
    replicate           Replicate backup snapshots to a remote server
    verify              Count the local snapshots and their age to check that everything is running well

optional arguments:
  -h, --help            show this help message and exit
  --verbose, -v
```

### backup-pve
```
usage: PROG backup-pve [-h] --remote REMOTE --prefix PREFIX --local-zfs-root LOCAL_ZFS_ROOT --remote-zfs-root REMOTE_ZFS_ROOT --remote-metadata-dir REMOTE_METADATA_DIR [--include-vms INCLUDE_VMS]
                       [--exclude-vms EXCLUDE_VMS] [--non-interactive] [--dry-run]

optional arguments:
  -h, --help            show this help message and exit
  --remote REMOTE       Remote server hostname
  --prefix PREFIX       Prefix to use in snapshot names for this backup job
  --local-zfs-root LOCAL_ZFS_ROOT
                        Dataset where local ZFS volumes are stored, for example rpool/data
  --remote-zfs-root REMOTE_ZFS_ROOT
                        Dataset in which to store backups on the remote server, for example main/backups
  --remote-metadata-dir REMOTE_METADATA_DIR
                        Folder in which to store metadata about the backups on the remote server, for example /root/backups_metadata
  --include-vms INCLUDE_VMS
                        Comma-separated list of VM ids to include in the backup. If not specified, all VMs will be backed up
  --exclude-vms EXCLUDE_VMS
                        Comma-separated list of VM ids to exclude from the backup. If not specified, all VMs will be backed up
  --non-interactive, -y
                        Assume yes to all questions
  --dry-run, -n
```

### prune
```
usage: PROG prune [-h] --local-zfs-root LOCAL_ZFS_ROOT --prefix PREFIX [--non-interactive] [-n] [--remove-many-is-ok] [--keep-last KEEP_LAST] [--keep-hourly KEEP_HOURLY] [--keep-daily KEEP_DAILY]
                  [--keep-weekly KEEP_WEEKLY] [--keep-monthly KEEP_MONTHLY] [--keep-yearly KEEP_YEARLY] [--remote REMOTE] [--remote-zfs-root REMOTE_ZFS_ROOT] [--only-preserve-latest-common]

optional arguments:
  -h, --help            show this help message and exit
  --local-zfs-root LOCAL_ZFS_ROOT
                        Where to start pruning
  --prefix PREFIX       Only prune snapshots that adhere to this backup prefix.
  --non-interactive, -y
                        Assume yes to all questions
  -n, --dry-run
  --remove-many-is-ok
  --keep-last KEEP_LAST
  --keep-hourly KEEP_HOURLY
  --keep-daily KEEP_DAILY
  --keep-weekly KEEP_WEEKLY
  --keep-monthly KEEP_MONTHLY
  --keep-yearly KEEP_YEARLY
  --remote REMOTE       If specified, we will connect to the server and check which snapshots have already been copied there. Snapshots that have not been copied yet will be preserved from the purge.
  --remote-zfs-root REMOTE_ZFS_ROOT
                        If --remote is set, where to look for remote snapshots
  --only-preserve-latest-common
                        When --remote is set, normally all snapshots that have not been synced to remote are preserved. With this flag, only the latest common snapshot is preserved.
```

### replicate
```
usage: PROG replicate [-h] [--non-interactive] [--only-last] [-n] --remote REMOTE --remote-zfs-root REMOTE_ZFS_ROOT --local-zfs-root LOCAL_ZFS_ROOT [--include-vms INCLUDE_VMS] [--exclude-vms EXCLUDE_VMS]

optional arguments:
  -h, --help            show this help message and exit
  --non-interactive, -y
                        Assume yes to all questions
  --only-last           By default the replicate command copies every snapshot to the remote. With this option only the latest snapshot is copied
  -n, --dry-run
  --remote REMOTE       Remote server hostname
  --remote-zfs-root REMOTE_ZFS_ROOT
                        If --remote is set, where to look for remote snapshots
  --local-zfs-root LOCAL_ZFS_ROOT
                        Where to start pruning
  --include-vms INCLUDE_VMS
                        Comma-separated list of VM ids to include in the backup. If not specified, all VMs will be backed up
  --exclude-vms EXCLUDE_VMS
                        Comma-separated list of VM ids to exclude from the backup. If not specified, all VMs will be backed up
```

### verify
```
usage: PROG verify [-h] --prefix PREFIX [--min-snapshots MIN_SNAPSHOTS] [--max-age-days MAX_AGE_DAYS] --local-zfs-root LOCAL_ZFS_ROOT [--include-vms INCLUDE_VMS] [--exclude-vms EXCLUDE_VMS]

optional arguments:
  -h, --help            show this help message and exit
  --prefix PREFIX       Only verify snapshots that adhere to this backup prefix.
  --min-snapshots MIN_SNAPSHOTS
                        We expect to have at least n backups for each volume
  --max-age-days MAX_AGE_DAYS
                        The latest backup for each volume should be less than x days old
  --local-zfs-root LOCAL_ZFS_ROOT
                        Where to start pruning
  --include-vms INCLUDE_VMS
                        Comma-separated list of VM ids to include in the backup. If not specified, all VMs will be backed up
  --exclude-vms EXCLUDE_VMS
                        Comma-separated list of VM ids to exclude from the backup. If not specified, all VMs will be backed up
```


## Support

Contact me through my website: [Guillaume Matheron](https://blog.guillaumematheron.fr)


## Roadmap

- [ ] Add advanced verification by booting the VM in a sandbox and extracting the syslog
- [ ] Add built-in restore capabilities

## Contributing

Any contributions are welcome through pull requests. Feel free to contact me or open an issue first to discuss.


## License
CC0

