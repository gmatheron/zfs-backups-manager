from tools.util import send_file
from zbm_types import DRY_RUN, RemoteServer


def send_vm_config(
    vmid: str, file_id: str, remote_server: RemoteServer, remote_metadata_dir: str
) -> None:
    config_file: str = f"/etc/pve/local/qemu-server/{vmid}.conf"

    if DRY_RUN[0]:
        print(f"Would send config file {config_file} to {remote_server}")
        return

    try:
        send_file(
            config_file,
            f"{vmid}-{file_id}.conf",
            remote_server.hostname,
            remote_metadata_dir,
        )
    except RuntimeError as e:
        if "No such file or directory" in str(e):
            raise RuntimeError(
                f"Unable to copy VM configuration to {remote_metadata_dir}.\n"
                f"Hint: mkdir {remote_metadata_dir}"
            )
        else:
            raise e
