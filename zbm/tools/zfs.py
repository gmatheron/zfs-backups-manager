import logging
import subprocess
from typing import Dict, List, NoReturn, Optional, Set

import click
import regex
import tqdm

from tools.processes import close_print_err, preexec_fn, run
from tools.util import parse_table
from zbm_types import DRY_RUN, DangerLevel

logger = logging.getLogger(__name__)


def list_snapshots(ds, ignore_if_not_exists: bool = False) -> List[Dict[str, str]]:
    """Return a list of snapshots of a given volume"""

    fields: List[str] = [
        "name",
        "type",
        "avail",
        "used",
        "usedsnap",
        "usedds",
        "usedrefreserv",
        "usedchild",
    ]
    try:
        return parse_table(
            run(
                "zfs",
                ["list", "-H", "-t", "snapshot", "-o", ",".join(fields), ds],
                danger_level=DangerLevel.NO_EFFECT,
            ),
            fields,
        )
    except RuntimeError as e:
        if ignore_if_not_exists and ("dataset does not exist" in str(e)):
            return []
        else:
            raise e


def list_volumes(root: str, types: Set[str] = {"volume"}) -> List[Dict[str, str]]:
    """Return a list of volumes at a given root"""

    fields: List[str] = [
        "name",
        "type",
        "avail",
        "used",
        "usedsnap",
        "usedds",
        "usedrefreserv",
        "usedchild",
    ]
    return sum(
        [
            parse_table(
                run(
                    "zfs",
                    ["list", "-H", "-r", "-t", t, "-o", ",".join(fields), root],
                    danger_level=DangerLevel.NO_EFFECT,
                ),
                fields,
            )
            for t in types
        ],
        [],
    )


def ordered_snapshot_numbers(snapshots: List[Dict], bak_prefix: str) -> List[int]:
    """
    Return a list of numbers for which there is a snapshot
    with the given prefix.
    For instance if the prefix is bak and the following shapshots exist:
     - abc123
     - bak3
     - bak1
    Then this function returns [1, 3]
    """

    return list(
        sorted(
            [
                int(snap["name"].split("@" + bak_prefix)[1])
                for snap in snapshots
                if "@" + bak_prefix in snap["name"]
            ]
        )
    )


def ordered_snapshot_strings(snapshots: List[Dict], bak_prefix: str) -> List[str]:
    """
    Return a list of strings for which there is a snapshot
    with the given prefix.
    For instance if the prefix is bak and the following shapshots exist:
     - abc123
     - bak3x
     - bak2
    Then this function returns ["123", "2", "3x"]
    """

    return list(
        sorted(
            [
                snap["name"].split("@" + bak_prefix)[1]
                for snap in snapshots
                if "@" + bak_prefix in snap["name"]
            ]
        )
    )


def delete_snapshot(full_name: str, force: bool = False) -> None:
    if "@" not in full_name:
        raise AssertionError(f"{full_name} is not a snapshot")

    if DRY_RUN[0]:
        print(f"Would destroy snapshot {full_name}")
        return

    if force or click.confirm(f"Destroy snapshot {full_name} ?", default=False):
        run("zfs", ["destroy", full_name], danger_level=DangerLevel.DANGEROUS)
    else:
        raise KeyboardInterrupt()


def delete_remote_snapshot(
    ssh_args: List[str], full_name: str, force: bool = False
) -> None:
    if "@" not in full_name:
        raise AssertionError(f"{full_name} is not a snapshot")

    if DRY_RUN[0]:
        print(f"Would destroy snapshot {full_name} on remote")
        return

    if force or click.confirm(f"Destroy REMOTE snapshot {full_name} ?", default=False):
        run(
            "ssh",
            ssh_args + ["zfs", "destroy", full_name],
            danger_level=DangerLevel.DANGEROUS,
        )
    else:
        raise KeyboardInterrupt()


def create_snapshot(full_name) -> None:
    run("zfs", ["snap", full_name], danger_level=DangerLevel.NORMAL)


def send_snapshot(
    source_with_snapshot: str,
    destination: str,
    source_base: Optional[str],
    ssh_args: List[str],
    *,
    raw: bool = False,  # Must be used for encrypted datasets
) -> None:
    """
    Send a snapshot to a remote system through SSH.
    If source_base is specified, perform an incremental send.
    """

    # Estimate size
    flags: Set[str] = {"-n", "-P"}
    if raw:
        flags.add("-w")
    zfs_nsend_args: List[str] = ["send"] + list(flags) + ["-R", source_with_snapshot]
    if source_base is not None:
        zfs_nsend_args = (
            ["send"]
            + list(flags)
            + [
                "-i",
                "@" + source_base,
                source_with_snapshot,
            ]
        )

    res: str = run("zfs", zfs_nsend_args, danger_level=DangerLevel.NORMAL)
    logger.debug(res)
    estimated_bytes: int = int(
        regex.sub(r"\s+", ";", res.split("\n")[-2]).split(";")[1]
    )

    flags: Set[str] = set()
    if raw:
        flags.add("-w")
    zfs_send_args: List[str] = (
        ["zfs", "send"] + list(flags) + ["-R", source_with_snapshot]
    )
    if source_base is not None:
        zfs_send_args = (
            ["zfs", "send"]
            + list(flags)
            + [
                "-i",
                "@" + source_base,
                source_with_snapshot,
            ]
        )
    ssh_args2: List[str] = ["ssh"] + ssh_args + ["zfs", "recv", "-F", destination]

    logger.info(" ".join(zfs_send_args) + " | " + " ".join(ssh_args2))

    zfs_send: subprocess.Popen[bytes] = subprocess.Popen(
        zfs_send_args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        preexec_fn=preexec_fn,
    )
    pbar: tqdm.tqdm[NoReturn] = tqdm.tqdm(
        unit="B",
        unit_scale=True,
        unit_divisor=1024,
        total=estimated_bytes,
        smoothing=0.1,
    )
    ssh: subprocess.Popen[bytes] = subprocess.Popen(
        ssh_args2,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        preexec_fn=preexec_fn,
    )
    assert zfs_send.stdout is not None
    assert ssh.stdin is not None
    assert ssh.stdout is not None
    i: int = 0
    try:
        while True:
            data: bytes = zfs_send.stdout.read(40960)
            if len(data) == 0:
                break
            ssh.stdin.write(data)
            pbar.update(len(data))
            i += len(data)
        pbar.close()
        ssh.stdin.close()
        logger.debug(f"End of loop after {i} bytes transferred")
    except BrokenPipeError:
        pbar.close()
        zfs_send.stdout.close()
        logger.debug(f"BPE after {i} bytes transferred")
    close_print_err(zfs_send, "zfs_send")
    close_print_err(ssh, "ssh")


def list_remote_snapshots(
    ssh_args: List[str], ds: str, ignore_if_not_exists=False
) -> List[Dict[str, str]]:
    fields: List[str] = [
        "name",
        "type",
        "avail",
        "used",
        "usedsnap",
        "usedds",
        "usedrefreserv",
        "usedchild",
    ]
    try:
        return parse_table(
            run(
                "ssh",
                ssh_args
                + [
                    "zfs",
                    "list",
                    "-H",
                    "-r",
                    "-t",
                    "snapshot",
                    "-o",
                    ",".join(fields),
                    ds,
                ],
                danger_level=DangerLevel.NO_EFFECT,
            ),
            fields,
        )
    except RuntimeError as e:
        if ignore_if_not_exists and ("dataset does not exist" in str(e)):
            return []
        else:
            raise e


def list_remote_volumes(
    ssh_args, root: str = "/", types: Set[str] = {"volume"}
) -> List[str]:
    fields: List[str] = [
        "name",
        "type",
        "avail",
        "used",
        "usedsnap",
        "usedds",
        "usedrefreserv",
        "usedchild",
    ]
    return [
        d["name"]
        for d in sum(
            [
                parse_table(
                    run(
                        "ssh",
                        ssh_args
                        + ["zfs", "list", "-H", "-r", "-t", t, "-o", ",".join(fields)]
                        + [root],
                        danger_level=DangerLevel.NO_EFFECT,
                    ),
                    fields,
                )
                for t in types
            ],
            [],
        )
    ]
