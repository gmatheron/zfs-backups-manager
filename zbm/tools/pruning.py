from typing import List

from tools.zfs import (delete_remote_snapshot, delete_snapshot,
                       list_remote_snapshots, list_snapshots,
                       ordered_snapshot_strings)
from zbm_types import DRY_RUN, NONINTERACTIVE


def keep_n_latest_snapshots(volume: str, prefix: str, n: int):
    assert n > 0

    if DRY_RUN[0]:
        print(
            f"Would keep {n} latest snapshots of {volume} that have "
            f"prefix {prefix}."
        )

    local_snaps = list_snapshots(volume)
    local_snap_ids = ordered_snapshot_strings(local_snaps, prefix)

    if len(local_snap_ids) > n:
        for i in local_snap_ids[:-n]:
            delete_snapshot(f"{volume}@{prefix}{i}", force=NONINTERACTIVE[0])


def keep_n_latest_remote_snapshots(
    ssh_args: List[str], volume: str, prefix: str, n: int
):
    assert n > 0

    if DRY_RUN[0]:
        print(
            f"Would keep {n} latest snapshots of {volume} that have "
            f"prefix {prefix}."
        )

    local_snaps = list_remote_snapshots(ssh_args, volume, True)
    local_snap_ids = ordered_snapshot_strings(local_snaps, prefix)

    if len(local_snap_ids) > n:
        for i in local_snap_ids[:-n]:
            delete_remote_snapshot(
                ssh_args, f"{volume}@{prefix}{i}", force=NONINTERACTIVE[0]
            )
