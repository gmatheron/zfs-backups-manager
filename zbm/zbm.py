import logging
from argparse import ArgumentParser

from commands.backup_pve import PVEBackupJob, backup_pve_vms
from commands.encrypt_and_backup import EncryptAndBackupJob, encrypt_and_backup
from commands.list import ListJob, list_backups
from commands.mount import MountJob, mount
from commands.prune import PruneJob, prune
from commands.replicate_backups import BackupReplicationJob, replicate_backups
from commands.verify import VerifyJob, verify
from zbm_types import DRY_RUN, NONINTERACTIVE, RemoteServer, VolumeBackupJob


def command_backup_pve(args):
    if args.non_interactive:
        NONINTERACTIVE[0] = True
    if args.dry_run:
        DRY_RUN[0] = True
    job = PVEBackupJob(
        remote_server=RemoteServer(hostname=args.remote),
        prefix=args.prefix,
        local_zfs_root=args.local_zfs_root,
        remote_zfs_root=args.remote_zfs_root,
        remote_metadata_dir=args.remote_metadata_dir,
        include_vms=[int(u) for u in args.include_vms.split(",")]
        if args.include_vms != ""
        else None,
        exclude_vms=[int(u) for u in args.exclude_vms.split(",")]
        if args.exclude_vms != ""
        else None,
        allow_encrypted=args.allow_encrypted,
    )
    backup_pve_vms(job)


def command_encrypt_and_backup(args) -> None:
    if args.non_interactive:
        NONINTERACTIVE[0] = True
    if args.dry_run:
        DRY_RUN[0] = True
    job = EncryptAndBackupJob(
        remote_server=RemoteServer(hostname=args.remote, port=args.remote_port),
        prefix=args.prefix,
        local_zfs_root=args.local_dataset,
        remote_zfs_root=args.remote_dataset,
        encrypted_dataset=args.encrypted_dataset,
        keyfile=args.keyfile,
        allow_encrypted=False,
    )
    encrypt_and_backup(job)


def command_replicate_backups(args):
    if args.non_interactive:
        NONINTERACTIVE[0] = True
    if args.dry_run:
        DRY_RUN[0] = True
    types = {"filesystem", "volume"}
    if args.only_filesystems:
        types = types - {"volume"}
    if args.only_volumes:
        types = types - {"filesystem"}
    job = BackupReplicationJob(
        remote_server=RemoteServer(hostname=args.remote, port=args.remote_port),
        local_zfs_root=args.local_zfs_root,
        remote_zfs_root=args.remote_zfs_root,
        prefix=args.prefix,
        include_vms=[int(u) for u in args.include_vms.split(",")]
        if args.include_vms != ""
        else None,
        exclude_vms=[int(u) for u in args.exclude_vms.split(",")]
        if args.exclude_vms != ""
        else None,
        only_last=args.only_last,
        types=types,
        is_encrypted=args.is_encrypted,
        allow_encrypted=True,
    )
    replicate_backups(job)


def command_prune(args):
    if args.non_interactive:
        NONINTERACTIVE[0] = True
    if args.dry_run:
        DRY_RUN[0] = True

    if args.remote is None:
        pbj = None
    else:
        pbj = VolumeBackupJob(
            remote_server=RemoteServer(hostname=args.remote, port=args.remote_port),
            prefix=args.prefix,
            local_zfs_root=args.local_zfs_root,
            remote_zfs_root=args.remote_zfs_root,
            allow_encrypted=True,
        )

    job = PruneJob(
        prefix=args.prefix,
        local_zfs_root=args.local_zfs_root,
        args=args,
        preserve_backup_job=pbj,
        only_preserve_latest_common=args.only_preserve_latest_common,
    )
    prune(job)


def command_verify_backups(args):
    types = {"volume"}
    if args.also_filesystems:
        types = types.union({"filesystem"})
    job = VerifyJob(
        prefix=args.prefix,
        local_zfs_root=args.local_zfs_root,
        min_snapshots=args.min_snapshots,
        max_age_days=args.max_age_days,
        types=types,
        include_vms=[int(u) for u in args.include_vms.split(",")]
        if args.include_vms != ""
        else None,
        exclude_vms=[int(u) for u in args.exclude_vms.split(",")]
        if args.exclude_vms != ""
        else None,
    )

    verify(job)


def command_list(args):
    job = ListJob(
        prefix=args.prefix,
        remote_dataset=args.dataset,
        remote_server=RemoteServer(hostname=args.remote, port=args.remote_port),
    )
    list_backups(job)


def command_mount(args):
    job = MountJob(
        remote_server=RemoteServer(hostname=args.remote, port=args.remote_port),
        remote_dataset_and_snapshot=args.dataset_and_snapshot,
        keyfile=args.keyfile,
    )
    mount(job)


def main():
    parser = ArgumentParser(prog="PROG")
    parser.add_argument("--verbose", "-v", action="store_true")

    subparsers = parser.add_subparsers(help="sub-command help", required=True)

    parser_backup_pve = subparsers.add_parser(
        "backup-pve",
        help=(
            "Backup PVE VMs based on ZFS volumes from the local system to a "
            "backup server through a SSH tunnel"
        ),
    )
    parser_backup_pve.add_argument(
        "--remote", type=str, help="Remote server hostname", required=True
    )
    parser_backup_pve.add_argument(
        "--prefix",
        type=str,
        help="Prefix to use in snapshot names for this backup job",
        required=True,
    )
    parser_backup_pve.add_argument(
        "--local-zfs-root",
        type=str,
        help="Dataset where local ZFS volumes are stored, for example rpool/data",
        required=True,
    )
    parser_backup_pve.add_argument(
        "--remote-zfs-root",
        type=str,
        help="Dataset in which to store backups on the remote server, for example main/backups",
        required=True,
    )
    parser_backup_pve.add_argument(
        "--remote-metadata-dir",
        type=str,
        help="Folder in which to store metadata about the backups on the remote server, for example /root/backups_metadata",
        required=True,
    )
    parser_backup_pve.add_argument(
        "--include-vms",
        type=str,
        help="Comma-separated list of VM ids to include in the backup. If not specified, all VMs will be backed up",
        default="",
    )
    parser_backup_pve.add_argument(
        "--exclude-vms",
        type=str,
        help="Comma-separated list of VM ids to exclude from the backup. If not specified, all VMs will be backed up",
        default="",
    )
    parser_backup_pve.add_argument(
        "--non-interactive",
        "-y",
        action="store_true",
        help="Assume yes to all questions",
    )
    parser_backup_pve.add_argument("--dry-run", "-n", action="store_true")
    parser_backup_pve.add_argument("--allow-encrypted", action="store_true")
    parser_backup_pve.set_defaults(func=command_backup_pve)

    parser_prune = subparsers.add_parser(
        "prune",
        help=(
            "Prune backup snapshots according to a schedule. Optionally preserve "
            "snapshots that have not yet been synced to an offsite storage server."
        ),
    )
    parser_prune.add_argument(
        "--local-zfs-root",
        type=str,
        help="Where to start pruning",
        required=True,
    )
    parser_prune.add_argument(
        "--prefix",
        type=str,
        help="Only prune snapshots that adhere to this backup prefix.",
        required=True,
    )
    parser_prune.add_argument(
        "--non-interactive",
        "-y",
        action="store_true",
        help="Assume yes to all questions",
    )
    parser_prune.add_argument("-n", "--dry-run", action="store_true")
    parser_prune.add_argument("--remove-many-is-ok", action="store_true")
    parser_prune.add_argument("--keep-last", type=int, default=0)
    parser_prune.add_argument("--keep-hourly", type=int, default=0)
    parser_prune.add_argument("--keep-daily", type=int, default=0)
    parser_prune.add_argument("--keep-weekly", type=int, default=0)
    parser_prune.add_argument("--keep-monthly", type=int, default=0)
    parser_prune.add_argument("--keep-yearly", type=int, default=0)
    parser_prune.add_argument(
        "--remote",
        type=str,
        help="If specified, we will connect to the server and check "
        "which snapshots have already been copied there. Snapshots that have "
        "not been copied yet will be preserved from the purge.",
    )
    parser_prune.add_argument(
        "--remote-port", type=int, help="Remote server port", default=22
    )
    parser_prune.add_argument(
        "--remote-zfs-root",
        type=str,
        help="If --remote is set, where to look for remote snapshots",
    )
    parser_prune.add_argument(
        "--only-preserve-latest-common",
        action="store_true",
        help=(
            "When --remote is set, normally all snapshots "
            "that have not been synced to remote are preserved. "
            "With this flag, only the latest common snapshot is preserved."
        ),
    )
    parser_prune.set_defaults(func=command_prune)

    parser_replicate = subparsers.add_parser(
        "replicate",
        help="Replicate backup snapshots to a remote server",
    )
    parser_replicate.add_argument(
        "--non-interactive",
        "-y",
        action="store_true",
        help="Assume yes to all questions",
    )
    parser_replicate.add_argument(
        "--only-filesystems",
        action="store_true",
        help="Ignore volumes",
    )
    parser_replicate.add_argument(
        "--only-volumes",
        action="store_true",
        help="Ignore filesystems",
    )
    parser_replicate.add_argument(
        "--is-encrypted",
        action="store_true",
        help="Sends raw stream (does not encrypt the dataset on its own)",
    )
    parser_replicate.add_argument(
        "--prefix",
        type=str,
        help="Only replicate snapshots that adhere to this backup prefix.",
        required=True,
    )
    parser_replicate.add_argument(
        "--only-last",
        action="store_true",
        help=(
            "By default the replicate command copies every snapshot to the remote. "
            "With this option only the latest snapshot is copied"
        ),
    )
    parser_replicate.add_argument("-n", "--dry-run", action="store_true")
    parser_replicate.add_argument(
        "--remote",
        type=str,
        help="Remote server hostname",
        required=True,
    )
    parser_replicate.add_argument(
        "--remote-port", type=int, help="Remote server port", default=22
    )
    parser_replicate.add_argument(
        "--remote-zfs-root",
        type=str,
        help="If --remote is set, where to look for remote snapshots",
        required=True,
    )
    parser_replicate.add_argument(
        "--local-zfs-root",
        type=str,
        help="Where to start pruning",
        required=True,
    )
    parser_replicate.add_argument(
        "--include-vms",
        type=str,
        help="Comma-separated list of VM ids to include in the backup. If not specified, all VMs will be backed up",
        default="",
    )
    parser_replicate.add_argument(
        "--exclude-vms",
        type=str,
        help="Comma-separated list of VM ids to exclude from the backup. If not specified, all VMs will be backed up",
        default="",
    )
    parser_replicate.set_defaults(func=command_replicate_backups)

    parser_verify = subparsers.add_parser(
        "verify",
        help=(
            "Count the local snapshots and their age to check "
            "that everything is running well"
        ),
    )
    parser_verify.add_argument(
        "--prefix",
        type=str,
        help="Only verify snapshots that adhere to this backup prefix.",
        required=True,
    )
    parser_verify.add_argument(
        "--min-snapshots",
        type=int,
        help="We expect to have at least n backups for each volume",
    )
    parser_verify.add_argument(
        "--max-age-days",
        type=int,
        help="The latest backup for each volume should be less than x days old",
    )
    parser_verify.add_argument(
        "--local-zfs-root",
        type=str,
        help="Where to start pruning",
        required=True,
    )
    parser_verify.add_argument(
        "--include-vms",
        type=str,
        help=(
            "Comma-separated list of VM ids to include in the backup. "
            "If not specified, all VMs will be backed up"
        ),
        default="",
    )
    parser_verify.add_argument(
        "--exclude-vms",
        type=str,
        help=(
            "Comma-separated list of VM ids to exclude from the backup. "
            "If not specified, all VMs will be backed up"
        ),
        default="",
    )
    parser_verify.add_argument(
        "--also-filesystems",
        action="store_true",
        help="Handle filesystems and volumes",
    )
    parser_verify.set_defaults(func=command_verify_backups)

    parser_encrypt_and_backup = subparsers.add_parser(
        "encrypt-and-backup",
        help=(
            "Encrypt a dataset and send the encrypted result to a remote ZFS server through SSH."
        ),
    )
    parser_encrypt_and_backup.add_argument(
        "--remote", type=str, help="Remote server hostname", required=True
    )
    parser_encrypt_and_backup.add_argument(
        "--remote-port", type=int, help="Remote server port", default=22
    )
    parser_encrypt_and_backup.add_argument(
        "--prefix",
        type=str,
        help="Prefix to use in snapshot names for this backup job",
        required=True,
    )
    parser_encrypt_and_backup.add_argument(
        "--local-dataset",
        type=str,
        help="Dataset to backup, for instance main/documents",
        required=True,
    )
    parser_encrypt_and_backup.add_argument(
        "--encrypted-dataset",
        type=str,
        help="Name to use for the encrypted dataset, for instance main/documents_enc",
        required=True,
    )
    parser_encrypt_and_backup.add_argument(
        "--remote-dataset",
        type=str,
        help="Dataset in which to store backups on the remote server, for example main/documents_enc",
        required=True,
    )
    parser_encrypt_and_backup.add_argument(
        "--keyfile",
        type=str,
        help="Encryption keyfile",
        required=True,
    )
    parser_encrypt_and_backup.add_argument(
        "--non-interactive",
        "-y",
        action="store_true",
        help="Assume yes to all questions",
    )
    parser_encrypt_and_backup.add_argument("--dry-run", "-n", action="store_true")
    parser_encrypt_and_backup.set_defaults(func=command_encrypt_and_backup)

    parser_list = subparsers.add_parser(
        "list",
        help="List available backups",
    )
    parser_list.add_argument(
        "--remote", type=str, help="Remote server hostname", required=True
    )
    parser_list.add_argument(
        "--remote-port", type=int, help="Remote server port", default=22
    )
    parser_list.add_argument(
        "--dataset",
        type=str,
    )
    parser_list.add_argument(
        "--prefix",
        type=str,
        help="Prefix to use in snapshot names for this backup job",
    )
    parser_list.set_defaults(func=command_list)

    parser_mount = subparsers.add_parser(
        "mount",
        help="Mount backups",
    )
    parser_mount.add_argument(
        "--remote", type=str, help="Remote server hostname", required=True
    )
    parser_mount.add_argument(
        "--remote-port", type=int, help="Remote server port", default=22
    )
    parser_mount.add_argument(
        "--dataset-and-snapshot",
        required=True,
        type=str,
    )
    parser_mount.add_argument(
        "--keyfile",
        type=str,
        help="Encryption keyfile",
    )
    parser_mount.set_defaults(func=command_mount)

    args = parser.parse_args()
    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.INFO,
        format="%(asctime)s %(levelname)-8s [%(name)-4s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    args.func(args)


if __name__ == "__main__":
    main()
