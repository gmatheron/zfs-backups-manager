import datetime
import logging
import os
from dataclasses import dataclass
from typing import List, Optional

from tools.encryption import create_key, encrypt_snapshot, print_key
from tools.pruning import (keep_n_latest_remote_snapshots,
                           keep_n_latest_snapshots)
from tools.zfs import (create_snapshot, delete_remote_snapshot,
                       delete_snapshot, list_remote_snapshots, list_snapshots,
                       ordered_snapshot_strings, send_snapshot)
from zbm_types import DRY_RUN, NONINTERACTIVE, VolumeBackupJob

logger = logging.getLogger(__name__)


@dataclass
class EncryptAndBackupJob(VolumeBackupJob):
    encrypted_dataset: str
    keyfile: str


def ensure_consistent(job: EncryptAndBackupJob) -> List[str]:
    plain_snaps = list_snapshots(job.local_zfs_root)
    enc_snaps = list_snapshots(job.encrypted_dataset, ignore_if_not_exists=True)
    remote_snaps = list_remote_snapshots(
        job.remote_server.ssh_args(), job.remote_zfs_root, True
    )

    plain_snap_numbers = ordered_snapshot_strings(plain_snaps, job.prefix)
    enc_snap_numbers = ordered_snapshot_strings(enc_snaps, job.prefix)
    remote_snap_numbers = ordered_snapshot_strings(remote_snaps, job.prefix)

    inter = set(plain_snap_numbers).intersection(
        set(enc_snap_numbers), set(remote_snap_numbers)
    )

    print(f"Local plain snapshots: {plain_snap_numbers}")
    print(f"Local enc snapshots: {enc_snap_numbers}")
    print(f"Remote enc snapshots: {remote_snap_numbers}")
    print(f"Intersection: {inter}")

    extra_snaps_in_plain = set(plain_snap_numbers) - inter
    if extra_snaps_in_plain != set():
        print(
            f"Found these snaps in plain that are not everywhere: {extra_snaps_in_plain}."
        )
        for i in extra_snaps_in_plain:
            delete_snapshot(
                f"{job.local_zfs_root}@{job.prefix}{i}", force=NONINTERACTIVE[0]
            )
        plain_snaps = list_snapshots(job.local_zfs_root)
        plain_snap_numbers = ordered_snapshot_strings(plain_snaps, job.prefix)

    extra_snaps_in_enc = set(enc_snap_numbers) - inter
    if extra_snaps_in_enc != set():
        print(
            f"Found these snaps in enc that are not everywhere: {extra_snaps_in_enc}."
        )
        for i in extra_snaps_in_enc:
            delete_snapshot(
                f"{job.encrypted_dataset}@{job.prefix}{i}", force=NONINTERACTIVE[0]
            )
        enc_snaps = list_snapshots(job.encrypted_dataset)
        enc_snap_numbers = ordered_snapshot_strings(enc_snaps, job.prefix)

    extra_snaps_in_remote = set(remote_snap_numbers) - inter
    if extra_snaps_in_remote != set():
        print(
            f"Found these snaps in remote that are not everywhere: {extra_snaps_in_remote}."
        )
        for i in extra_snaps_in_remote:
            delete_remote_snapshot(
                job.remote_server.ssh_args(),
                f"{job.remote_zfs_root}@{job.prefix}{i}",
                force=NONINTERACTIVE[0],
            )
        remote_snaps = list_remote_snapshots(
            job.remote_server.ssh_args(), job.remote_zfs_root
        )
        remote_snap_numbers = ordered_snapshot_strings(remote_snaps, job.prefix)

    return list(sorted(inter))


def ensure_consistent_local_enc(job: EncryptAndBackupJob) -> List[str]:
    plain_snaps = list_snapshots(job.local_zfs_root)
    enc_snaps = list_snapshots(job.encrypted_dataset, ignore_if_not_exists=True)

    plain_snap_numbers = ordered_snapshot_strings(plain_snaps, job.prefix)
    enc_snap_numbers = ordered_snapshot_strings(enc_snaps, job.prefix)

    inter = set(plain_snap_numbers).intersection(set(enc_snap_numbers))

    print(f"Local plain snapshots: {plain_snap_numbers}")
    print(f"Local enc snapshots: {enc_snap_numbers}")
    print(f"Intersection: {inter}")

    extra_snaps_in_plain = set(plain_snap_numbers) - inter
    if extra_snaps_in_plain != set():
        print(
            f"Found these snaps in plain that are not everywhere: {extra_snaps_in_plain}."
        )
        for i in extra_snaps_in_plain:
            delete_snapshot(
                f"{job.local_zfs_root}@{job.prefix}{i}", force=NONINTERACTIVE[0]
            )
        plain_snaps = list_snapshots(job.local_zfs_root)
        plain_snap_numbers = ordered_snapshot_strings(plain_snaps, job.prefix)

    extra_snaps_in_enc = set(enc_snap_numbers) - inter
    if extra_snaps_in_enc != set():
        print(
            f"Found these snaps in enc that are not everywhere: {extra_snaps_in_enc}."
        )
        for i in extra_snaps_in_enc:
            delete_snapshot(
                f"{job.encrypted_dataset}@{job.prefix}{i}", force=NONINTERACTIVE[0]
            )
        enc_snaps = list_snapshots(job.encrypted_dataset)
        enc_snap_numbers = ordered_snapshot_strings(enc_snaps, job.prefix)

    return list(sorted(inter))


def ensure_consistent_enc_remote(job: EncryptAndBackupJob) -> List[str]:
    enc_snaps = list_snapshots(job.encrypted_dataset, ignore_if_not_exists=True)
    remote_snaps = list_remote_snapshots(
        job.remote_server.ssh_args(), job.remote_zfs_root, True
    )

    enc_snap_numbers = ordered_snapshot_strings(enc_snaps, job.prefix)
    remote_snap_numbers = ordered_snapshot_strings(remote_snaps, job.prefix)

    inter = set(enc_snap_numbers).intersection(set(remote_snap_numbers))

    print(f"Local enc snapshots: {enc_snap_numbers}")
    print(f"Remote enc snapshots: {remote_snap_numbers}")
    print(f"Intersection: {inter}")

    extra_snaps_in_remote = set(remote_snap_numbers) - inter
    if extra_snaps_in_remote != set():
        print(
            f"Found these snaps in remote that are not everywhere: {extra_snaps_in_remote}."
        )
        for i in extra_snaps_in_remote:
            delete_remote_snapshot(
                job.remote_server.ssh_args(),
                f"{job.remote_zfs_root}@{job.prefix}{i}",
                force=NONINTERACTIVE[0],
            )
        remote_snaps = list_remote_snapshots(
            job.remote_server.ssh_args(), job.remote_zfs_root
        )
        remote_snap_numbers = ordered_snapshot_strings(remote_snaps, job.prefix)

    return list(sorted(inter))


def encrypt_and_backup(job: EncryptAndBackupJob):
    print(job)

    if not os.path.exists(job.keyfile):
        print("Creating key file, store it securely!")
        print()
        create_key(job.keyfile)
        print_key(job.keyfile)

    print("Using as key file: ", job.keyfile)

    base_ids = ensure_consistent_local_enc(job)
    base_id = max(base_ids) if len(base_ids) > 0 else None

    #### CREATE LOCAL SNAP ####

    new_id = datetime.datetime.utcnow().strftime("%Y%m%dT%H%MZ")
    local_new_snap = f"{job.local_zfs_root}@{job.prefix}{new_id}"
    if DRY_RUN[0]:
        print(f"Would create snapshot {local_new_snap}")
    else:
        logger.info("Creating snapshot " + local_new_snap)
        create_snapshot(local_new_snap)

    ### ENCRYPT LOCAL SNAP ###

    if base_id is None:
        print(f"Encrypt snapshot to {job.encrypted_dataset}")
        if DRY_RUN[0]:
            print("Dry run, simulating...")
        else:
            encrypt_snapshot(
                f"{job.local_zfs_root}@{job.prefix}{new_id}",
                job.encrypted_dataset,
                job.keyfile,
                None,
            )
    else:
        print(
            f"Encrypt snapshot to {job.encrypted_dataset} (using previous snapshot "
            f"{base_id} to perform a differential encrypt)"
        )
        if DRY_RUN[0]:
            print("Dry run, simulating...")
        else:
            encrypt_snapshot(
                f"{job.local_zfs_root}@{job.prefix}{new_id}",
                job.encrypted_dataset,
                job.keyfile,
                f"{job.prefix}{base_id}",
            )

    base_ids = ensure_consistent_enc_remote(job)
    base_id = max(base_ids) if len(base_ids) > 0 else None

    ### SEND SNAP ###

    local_snap = f"{job.encrypted_dataset}@{job.prefix}{new_id}"
    if base_id is None:
        if DRY_RUN[0]:
            print(f"Dry run, simulating sending {local_snap} ...")
        else:
            send_snapshot(
                local_snap,
                job.remote_zfs_root,
                None,
                job.remote_server.ssh_args(),
                raw=True,
            )
    else:
        if DRY_RUN[0]:
            print(f"Dry run, simulating sending {local_snap} with base {base_id}...")
        else:
            send_snapshot(
                local_snap,
                job.remote_zfs_root,
                f"{job.prefix}{base_id}",
                job.remote_server.ssh_args(),
                raw=True,
            )

    # TODO only if everything went well
    # keep_n_latest_snapshots(job.local_zfs_root, job.prefix, 1)
    # keep_n_latest_snapshots(job.encrypted_dataset, job.prefix, 1)
    # keep_n_latest_remote_snapshots(
    #     job.remote_server.ssh_args(), job.encrypted_dataset, job.prefix, 1
    # )
