import datetime
import logging
from dataclasses import dataclass
from typing import List, Optional, Set

from regex import regex

from tools.zfs import list_snapshots, list_volumes, ordered_snapshot_strings

logger = logging.getLogger(__name__)


@dataclass
class VerifyJob:
    prefix: str
    types: Set[str]
    local_zfs_root: str
    min_snapshots: Optional[int] = None
    max_age_days: Optional[int] = None
    include_vms: Optional[List[int]] = None
    exclude_vms: Optional[List[int]] = None


def verify(job: VerifyJob):
    logger.info(f"Starting backup verification job {job}")

    volumes = [v["name"] for v in list_volumes(job.local_zfs_root, types=job.types)]
    logger.debug("== LOCAL VOLUMES ==")
    logger.debug(volumes)
    logger.debug("")

    all_ok = True

    for local_volume in volumes:
        if job.exclude_vms is not None or job.include_vms is not None:
            vmid: str = regex.sub(r"^.*/vm-([0-9]+)-disk-[0-9]+$", f"\\1", local_volume)
            if vmid == local_volume:
                logger.debug(
                    f"Skipping volume {local_volume} that does not seem to belong to a VM"
                )
                continue

            if job.exclude_vms is not None and int(vmid) in job.exclude_vms:
                logger.debug(f"Skipping excluded VM {vmid}")
                continue

            if job.include_vms is not None and int(vmid) not in job.include_vms:
                logger.debug(f"Skipping non-included VM {vmid}")
                continue

        local_snaps = list_snapshots(local_volume)
        logger.debug(f"Local snapshots: {local_snaps}")
        local_snap_ids = ordered_snapshot_strings(local_snaps, job.prefix)

        if job.min_snapshots is not None:
            if len(local_snap_ids) < job.min_snapshots:
                logger.warn(
                    f"Volume {local_volume} only has {len(local_snap_ids)} "
                    f"snapshots instead of {job.min_snapshots}."
                )
                all_ok = False
            else:
                logger.info(
                    f"Volume {local_volume} has {len(local_snap_ids)} snapshots."
                )

        if job.max_age_days is not None:
            if len(local_snap_ids) == 0:
                logger.warn("Volume {local_volume} has no snapshot.")
                all_ok = False
            else:
                last_snap_id = max(local_snap_ids)
                last_snap_datetime = datetime.datetime.strptime(
                    last_snap_id, "%Y%m%dT%H%MZ"
                )
                age = datetime.datetime.utcnow() - last_snap_datetime
                if age.days > job.max_age_days:
                    logger.warn(
                        f"The last snapshot of volume {local_volume} "
                        f"is {age.days} days old, which is more "
                        f"than {job.max_age_days}."
                    )
                else:
                    logger.info(
                        f"The last snapshot of volume {local_volume} "
                        f"is {age.days} days old."
                    )

    return all_ok
