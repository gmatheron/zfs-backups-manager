import argparse
import logging
from dataclasses import dataclass
from typing import List, Optional

import regex

from tools.retention import apply_retention_rules
from tools.zfs import (delete_snapshot, list_remote_snapshots, list_snapshots,
                       list_volumes, ordered_snapshot_strings)
from zbm_types import DRY_RUN, NONINTERACTIVE, VolumeBackupJob

logger = logging.getLogger(__name__)


@dataclass
class PruneJob:
    prefix: str
    local_zfs_root: str
    args: argparse.Namespace
    preserve_backup_job: Optional[VolumeBackupJob]
    only_preserve_latest_common: bool


def prune(job: PruneJob):
    volumes = [
        v["name"]
        for v in list_volumes(job.local_zfs_root, types={"volume", "filesystem"})
    ]
    for local_vol in volumes:
        logger.info(f"Handling {local_vol}")

        try:
            snaps: List[str] = ordered_snapshot_strings(
                list_snapshots(local_vol), bak_prefix=job.prefix
            )
        except RuntimeError:
            logger.warning(
                f"Volume {local_vol} disappeared before we could get to it ? Skipping."
            )
            continue
        logger.info(f"Local snapshots: {len(snaps)}")
        logger.debug(f"Local snapshots: {snaps}")

        if len(snaps) == 0:
            logger.info(f"Could not find a single backup snap for {local_vol}")
            continue

        keep = apply_retention_rules(snaps, job.args)
        logger.info(f"Rules say we keep {len(keep)} snapshots")

        if job.preserve_backup_job is not None:
            remote_volume: str = regex.sub(
                r"^(.*)/([^/]*)$",
                f"{job.preserve_backup_job.remote_zfs_root}/\\2",
                local_vol,
            )
            try:
                remote_snaps = ordered_snapshot_strings(
                    list_remote_snapshots(
                        job.preserve_backup_job.remote_server.ssh_args(), remote_volume
                    ),
                    bak_prefix=job.prefix,
                )
            except RuntimeError as e:
                if "dataset does not exist" in str(e):
                    if job.only_preserve_latest_common:
                        remote_snaps = []
                    else:
                        logger.warning(
                            f"Unable to find remote dataset {remote_volume}, will not prune anything."
                        )
                        continue
                else:
                    raise e
            logger.info(f"Remote snapshots: {len(remote_snaps)}")
            logger.debug(f"Remote snapshots: {remote_snaps}")

            not_yet_synced = set(snaps) - set(remote_snaps)
            sorted_common = sorted(set(snaps) & set(remote_snaps))
            if len(sorted_common) > 0:
                latest_common = {sorted_common[-1]}
            else:
                latest_common = set()
            logger.info(f"Not yet synced: {len(not_yet_synced)}")
            logger.debug(f"Not yet synced: {not_yet_synced}")
            logger.info(f"Latest common: {latest_common}")

            if job.only_preserve_latest_common:
                keep = keep | latest_common
            else:
                keep = keep | latest_common | not_yet_synced
            logger.info(
                f"Bump the number of kept snapshots to {len(keep)} "
                "because of preserved backup job"
            )

        if len(keep) < len(snaps) * 0.2 and not job.args.remove_many_is_ok:
            print(
                f" /!\\ Asking to remove {len(snaps) - len(keep)} out of "
                f"{len(snaps)} snaps, that is suspicious... Skipping."
            )
            continue

        for snap in sorted(snaps):
            snap_full = local_vol + "@" + job.prefix + snap
            if snap not in keep:
                logger.debug(f"REMOVE snapshot {snap_full}")
                if DRY_RUN[0]:
                    print(f"Would REMOVE {snap_full}")
                else:
                    delete_snapshot(snap_full, force=NONINTERACTIVE[0])
            else:
                logger.debug(f"KEEP   snapshot {snap_full}")
                if DRY_RUN[0]:
                    print(f"Would KEEP {snap_full}")
